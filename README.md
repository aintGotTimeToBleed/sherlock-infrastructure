# Sherlock Infrastructure

## minikube (on ubuntu)
prerequisites:
- minikube (v1.32.0)
- helm (v3.14.4)
- docker (v26.0.1)
- strimzi (v0.41.0)
- JDK11
- JDK17
- flink kubernetes operator (v1.8.0)

*** prepare local directory for storing state
```
sudo mkdir /state
sudo chown -R 9999:9999 /state
sudo chmod -R 777 /state
```
*** start minikube
```
minikube start --memory=20480 --cpus=4 --driver=virtualbox --disk-size='30000mb'
### open as separate process
minikube mount --gid=9999 --uid=9999 /state:/state
### we need run tunnel command in order to expose kafka, api etc. 
### kafka user wont be created without that
### open as separate process
minikube tunnel
```
*** push necessary docker images to minikube docker registry
*** in case of errors -> http://computerbryan.com/minikube-on-ubuntu.html
*** (sherlock project) build elasticsearch image, api, history-api etc
```
eval $(minikube docker-env)
docker build docker/elasticsearch/. -t pl.mzapisek.sherlock/elasticsearch7:0.1.0
JAVA_HOME=/usr/lib/jvm/java-1.17.0-openjdk-amd64 mvn clean install -Pgitlab -DskipTests
```
*** (linker project) build linker docker image
```
eval $(minikube docker-env)
JAVA_HOME=/usr/lib/jvm/java-1.17.0-openjdk-amd64 ./gradlew clean build docker -x test
```
*** (sherlock-claims project) build sherlock-claims image
```
eval $(minikube docker-env)
JAVA_HOME=/usr/lib/jvm/java-1.11.0-openjdk-amd64 mvn clean install -DskipTests
```
*** deploy infrastructure (from minikube directory)
```
# kafka
kubectl create namespace sherlock
kubectl config set-context --current --namespace=sherlock
helm repo add strimzi https://strimzi.io/charts/
helm repo update
helm install strimzi strimzi/strimzi-kafka-operator --version 0.41.0 -n sherlock

# if kafka doesnt want to start make sure that minikube tunnel command is working properly
kubectl apply -f 0_kafka.yaml
# create topics (this file should be kept up to data according to topic changes)
kubectl apply -f 0_create_topics.yaml 
# postgres
kubectl apply -f 0_postgres.yaml
# mysql
kubectl apply -f 0_mysql.yaml
# elasticsearch
kubectl apply -f 0_elasticsearch.yaml
# flink operator
kubectl create -f https://github.com/jetstack/cert-manager/releases/download/v1.14.5/cert-manager.yaml
# wait some time for cert-manager to be created
helm repo add flink-operator-repo https://downloads.apache.org/flink/flink-kubernetes-operator-1.8.0/
helm install -f flink_operator_values.yaml flink-kubernetes-operator flink-operator-repo/flink-kubernetes-operator --version 1.8.0

# deploy prometheus/grafana
helm repo add prometheus-community https://prometheus-community.github.io/helm-charts
helm install prometheus prometheus-community/kube-prometheus-stack --version 57.2.1
kubectl apply -f 0_monitoring.yaml
kubectl port-forward deployment/prometheus-grafana 3000
kubectl port-forward prometheus-prometheus-kube-prometheus-prometheus-0 9095:9090

grafana login/password: admin / prom-operator

```
*** deploy applications
```
kubectl apply -f 1_sherlock_api.yaml
kubectl apply -f 1_history_api.yaml
kubectl apply -f 1_rules_calculator.yaml
kubectl apply -f 1_linker.yaml
kubectl apply -f 2_sherlock_claims.yaml
```

*** useful
** flink UI
```
kubectl port-forward sherlock-claims-deployment-688cc4fb6-6gppq 8081:8081
```
** kafka pki certificate
```
### in order to connect to kafka we need to use generated certificate and password as well as generated kafka user
### copy cert from kafka-cluster-ca-cert secret (kafka namespace) and put it in applications config maps
### copy kafka user (kafka-sherlock-user from kafka namespace) and put it in applications config maps
kubectl get secret kafka-cluster-ca-cert -o "jsonpath={.data['ca\.p12']}" | base64 -d > /home/zapisek/cert.p12
### other kafka related commands
kubectl get secret kafka-cluster-ca-cert -o "jsonpath={.data['ca\.password']}" | base64 -d
kubectl -n kafka get secret kafka-cluster-ca-cert -o yaml
kubectl get secret kafka-sherlock-user -o yaml
```

### todo
```
# !!! optional, in progress
# install prometheus/grafana

https://devopscube.com/setup-prometheus-monitoring-on-kubernetes/
https://devopscube.com/setup-grafana-kubernetes/

https://mahira-technology.medium.com/how-to-set-up-minikube-prometheus-and-grafana-for-monitoring-your-application-performance-a-808a52b0812f
https://medium.com/@sushantkapare1717/setup-prometheus-monitoring-on-kubernetes-using-grafana-fe09cb3656f7

helm repo add prometheus-community https://prometheus-community.github.io/helm-charts
helm repo update
helm install prometheus prometheus-community/prometheus
helm install -f prometheus.yaml sherlock-prometheus prometheus-community/prometheus
```

### clean ups
```
kubectl -n kafka delete $(kubectl get strimzi -o name -n kafka)
kubectl -n kafka delete -f 'https://strimzi.io/install/latest?namespace=kafka'
kubectl delete namespace
```

### Useful 
## debugging
https://www.youtube.com/watch?v=bhcFfS1-eDY&list=PLDX4T_cnKjD0J2LFr7yBk2aSS_o2l-7ue&index=13
## 