apiVersion: v1
kind: Service
metadata:
  name: sherlock-api
spec:
  selector:
    app: sherlock-api
  ports:
    - name: app
      protocol: TCP
      port: 8077
      targetPort: 8077
    - name: actuator
      protocol: TCP
      port: 8078
      targetPort: 8078
  type: LoadBalancer
---
apiVersion: apps/v1
kind: StatefulSet
metadata:
  name: sherlock-api
  labels:
    app: sherlock-api
spec:
  serviceName: sherlock-api
  replicas: 1
  selector:
    matchLabels:
      app: sherlock-api
  template:
    metadata:
      labels:
        app: sherlock-api
    spec:
      containers:
        - name: sherlock-api
          image: pl.mzapisek.sherlock/api:0.1.0
          imagePullPolicy: "Never"
          ports:
            - name: app
              containerPort: 8077
            - name: actuator
              containerPort: 8078
          env:
            - name: SPRING_CONFIG_LOCATION
              value: "/app/config/application.yml"
            - name: KAFKA_CERT_PASSWORD
              valueFrom:
                secretKeyRef:
                  name: kafka-cluster-ca-cert
                  key: ca.password
            - name: KAFKA_USER_JAAS_CONFIG
              valueFrom:
                secretKeyRef:
                  name: kafka-sherlock-user
                  key: sasl.jaas.config
          volumeMounts:
            - name: sherlock-api-config
              mountPath: /app/config
            - name: sherlock-kafka-cert-secret
              mountPath: /app/cert
              readOnly: true
      volumes:
        - name: sherlock-api-config  #3
          configMap:
            # Provide the name of the ConfigMap you want to mount.
            name: sherlock-api-config #1
            # An array of keys from the ConfigMap to create as files
            items:
              - key: "application.yml" #2
                #this should match with the key in ConfigMap
                path: "application.yml" # mount path
        - name: sherlock-kafka-cert-secret
          secret:
            secretName:  kafka-cluster-ca-cert
            items:
              - key: ca.p12
                path: ca.p12
---
apiVersion: v1
kind: ConfigMap
metadata:
  name: sherlock-api-config
data:
  application.yml: |
    instance-id: ${HOSTNAME}
    server:
      port: 8077
    spring:
      kafka:
        bootstrap-servers: kafka-kafka-bootstrap.sherlock.svc:9093

        properties:
          ssl:
            truststore:
              location: /app/cert/ca.p12
              password: ${KAFKA_CERT_PASSWORD}
              type: PKCS12
          security:
            protocol: SASL_SSL
          sasl:
            mechanism: SCRAM-SHA-512
            jaas:
              config: ${KAFKA_USER_JAAS_CONFIG}
    
        producer:
          value-serializer: org.apache.kafka.common.serialization.ByteArraySerializer
          batch-size: 50
          acks: all
        consumer:
          value-deserializer: org.apache.kafka.common.serialization.ByteArrayDeserializer
          group-id: ${HOSTNAME}-consumer-group
          auto-offset-reset: earliest
    topics:
      results: results
      commands: commands
    response:
      timeout: 5
    management:
      server:
        port: 8078
      endpoints:
        web:
          exposure:
            include: health,info,prometheus,metrics
      metrics:
        enable:
          all: false
          tomcat: false
          jvm: false
          process: false
          hikaricp: false
          system: false
          jdbc: false
          http: false
          logback: false
          kafka: true
          application: false
          executor: false
          jmx: false
          spring: false
          sherlock: true
